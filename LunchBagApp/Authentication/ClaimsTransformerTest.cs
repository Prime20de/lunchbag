﻿using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Authorization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Security.Cryptography;
using System.Text;
using Microsoft.AspNetCore.Authentication;
using System.Security.Principal;
using LunchBagApp.Entities;
using LunchBagApp.Data;
using Microsoft.Extensions.Logging;

namespace LunchBagApp.Authentication
{
    public class ClaimsTransformerTest : IClaimsTransformation
    {
        LunchBagContext _lunchBagContext;
        private readonly ILogger _logger;

        public ClaimsTransformerTest(LunchBagContext lunchBagContext, ILogger<ClaimsTransformerTest> logger)
        {
            _lunchBagContext = lunchBagContext;
            _logger = logger;
        }

        private string _usernameHashed = "";

        public string AssignUser(string windowsUsername)
        {
            _usernameHashed = HashUsername(windowsUsername);
            string nickname = "";
            var uniqueUser = _lunchBagContext.User
                .FirstOrDefault(u => u.UniqueId == _usernameHashed);

            //First login
            if (uniqueUser == null)
            {
                nickname = AssignPokemon(_usernameHashed);
                CreateDbUser(_usernameHashed, nickname);
            }
            else
            {
                nickname = _lunchBagContext.User
                .First(u => u.UniqueId == _usernameHashed)
                .Nickname;
            }
            return nickname;
        }

        public string HashUsername(string username)
        {
            using (SHA1 sha1 = SHA1.Create())
            {
                byte[] bytes = sha1.ComputeHash(Encoding.UTF8.GetBytes(username));

                StringBuilder builder = new StringBuilder();
                for (int i = 0; i < bytes.Length; i++)
                {
                    builder.Append(bytes[i].ToString("x2"));
                }
                return builder.ToString();
            }
        }

        public string AssignPokemon(string hashname)
        {
            string adjective = "";
            string pokemon = "";
            bool foundNew = false;
            //Choose Pokemon/adjective at random, check if already used.
            while (!foundNew)
            {
                pokemon = RandomPokemon();
                adjective = RandomAdjective();

                var nick = _lunchBagContext.User
                    .FirstOrDefault(u => u.Nickname == adjective + " " + pokemon);
                foundNew = nick == null;
            }
            return adjective + " " + pokemon;
        }

        public string RandomPokemon()
        {
            var rnd = new Random().Next(_lunchBagContext.Pokemon.Count());
            var poke = _lunchBagContext.Pokemon
                .Skip(rnd)
                .FirstOrDefault();

            return poke.Name;
        }

        public void CreateDbUser(string uniqueID, string nickname)
        {
            var User = new User();
            User.UniqueId = uniqueID;
            User.Nickname = nickname;
            _lunchBagContext.User.Add(User);
            _lunchBagContext.SaveChanges();
        }

        public string RandomAdjective()
        {
            var rnd = new Random().Next(_lunchBagContext.Adjective.Count());
            var adj = _lunchBagContext.Adjective
                .Skip(rnd)
                .FirstOrDefault();

            return adj.Name;
        }

        public Task<ClaimsPrincipal> TransformAsync(ClaimsPrincipal principal)
        {
            
                var windowsIdentity = (ClaimsIdentity)principal.Identity;

                var identity = new ClaimsIdentity();
            try
            {
                if (windowsIdentity.IsAuthenticated && windowsIdentity is WindowsIdentity)
                {
                    var nickname = AssignUser(windowsIdentity.Name);
                    windowsIdentity.AddClaim(new Claim("Nickname", nickname));
                }
            }
            catch (Exception e)
            {
                _logger.LogError(e.ToString());
            }
            return Task.FromResult(new ClaimsPrincipal(windowsIdentity));
        }
    }
}
