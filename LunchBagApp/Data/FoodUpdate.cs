﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using LunchBagApp.Entities;
using System.Text.RegularExpressions;

namespace LunchBagApp.Data
{
    public class FoodUpdate
    {
        LunchBagContext _lunchBagContext;
        public FoodUpdate(LunchBagContext lunchBagContext)
        {
            _lunchBagContext = lunchBagContext;
        }
        public void UpdateFood(int weekDiff)
        {
            /*
             * INFORMATION REMOVED
             *  
             */
            //string url = "URL REMOVED" + weekDiff * 7;
            string url = "URL REMOVED";
            var web = new HtmlWeb();
            HtmlDocument doc = web.Load(url);

            string category = "";
            string name = "";
            string price = "";
            string dayDate = "";
            //string monthYear = "";
            string day = "";
            //Week start and Month won't change
            //*[@id="menu_items"]/div/div[1]/div[1]/h2
            dayDate = doc.DocumentNode.SelectNodes("//*[@id=\"menu_items\"]/div/div[1]/div[1]/h2")[0].InnerText;
            //monthYear = doc.DocumentNode.SelectNodes("//*[@id=\"date_top_content\"]")[0].InnerText;

            //Get WeekStartDate
            int weekStartDay = GetDate(dayDate);
            int month = GetMonth(dayDate.TrimStart().Split(' ')[1].Trim());
            // Have to set this by current year since they removed the year from their menu.
            int year = DateTime.Today.Year;
            DateTime date = new DateTime(year, month, weekStartDay);
            var dat = _lunchBagContext.Week
                    .FirstOrDefault(d => d.StartDate.Equals(date));
            if (dat == null)
            {
                CreateDbWeek(date);
            }
            var weekObj = _lunchBagContext.Week
                .First(w => w.StartDate.Equals(date));

            foreach (HtmlNode col in doc.DocumentNode.SelectNodes("//*[@id=\"menu_items\"]/div/div"))
            {
                //Every Day as col
                var dateOfDay = col.ChildNodes[0].ChildNodes[0].InnerText;
                day = dateOfDay.Trim().Split(',')[0].Trim();
                if (NotADay(day))
                {
                    break;
                }
                var dayObj = _lunchBagContext.Day
                    .First(d => d.Name.Equals(day));
                
                foreach (HtmlNode row in col.SelectNodes("div[2]/*"))
                {
                    //The rows alternate between the category and its actual food for the day
                    if(row.Name.Equals("h3"))
                    {
                        category = row.InnerText;
                        if (category.Equals("center"))
                        {
                            // Legacy from old page layout
                            break;
                        }
                        continue;
                    }
                    //Get Category/Food/Price for one Day, row is food                    
                    name = row.ChildNodes[0].ChildNodes[0].InnerText;
                    price = row.ChildNodes[0].ChildNodes[1].InnerText;

                    name = Unify(name);
                    var cat = _lunchBagContext.Category
                        .FirstOrDefault(c => c.Name.Equals(category));
                    var fud = _lunchBagContext.Food
                        .FirstOrDefault(f => f.Name.Equals(name) && f.Category.Name.Equals(category));
                    //New Food/Category
                    if (cat == null)
                    {
                        CreateDbCategory(category);
                    }
                    var categoryObj = _lunchBagContext.Category
                        .First(c => c.Name.Equals(category));
                    if (fud == null)
                    {
                        CreateDbFood(name, categoryObj, price);
                    }
                    var foodObj = _lunchBagContext.Food
                        .First(f => f.Name.Equals(name) && f.Category.Equals(categoryObj));
                    //Check WeekDayFoodTable, act accordingly
                    var wdf = _lunchBagContext.WeekDayFood
                        .FirstOrDefault(x => x.Day.Equals(dayObj) && x.Week.Equals(weekObj) && x.Food.Equals(foodObj));
                    if (wdf == null)
                    {
                        CreateDbWDF(dayObj, weekObj, foodObj);
                    }
                }
            }
        }

        private bool NotADay(string day)
        {
            return day switch
            {
                "Monday" => false,
                "Tuesday" => false,
                "Wednesday" => false,
                "Thursday" => false,
                "Friday" => false,
                _ => true,
            };
        }

        private int GetDate(string date)
        {
            string[] numbers = Regex.Split(date, @"\D+");
            return Convert.ToInt32(numbers[1]);
        }

        private string SeperateMonth(string month)
        {
            string[] months = Regex.Split(month, @"\d");
            return months[0];
        }

        //DbEntry for Food
        private void CreateDbFood(string name, Category category, string price)
        {
            var food = new Food();
            food.Category = category;
            food.Name = name;
            char[] prices = price.ToArray();
            food.Price = string.Join("", prices);
            _lunchBagContext.Food.Add(food);
            _lunchBagContext.SaveChanges();
        }

        //DbEntry for Category
        private void CreateDbCategory(string _name)
        {
            var _category = new Category();
            _category.Name = _name;
            _lunchBagContext.Category.Add(_category);
            _lunchBagContext.SaveChanges();
        }

        //DbEntry for Week
        private void CreateDbWeek(DateTime date)
        {
            var _Week = new Week();
            _Week.StartDate = date;
            _lunchBagContext.Week.Add(_Week);
            _lunchBagContext.SaveChanges();
        }

        //DbEntry for WeekDayFood
        private void CreateDbWDF(Day day, Week week, Food food)
        {
            var wdf = new WeekDayFood();
            wdf.Day = day;
            wdf.Week = week;
            wdf.Food = food;
            _lunchBagContext.WeekDayFood.Add(wdf);
            _lunchBagContext.SaveChanges();
        }

        private string Unify(string name)
        {
            name = name.ToLower();
            name = name.Replace(',', '&');
            if (name.Contains(" with"))
            {
                int position = name.IndexOf(" with");
                name = name.Remove(position, " with".Length);
                name = name.Insert(position, "&");
                name = name.Insert(position, " ");
            }
            if (name.Contains(" and"))
            {
                int position = name.IndexOf(" and");
                name = name.Remove(position, " and".Length);
                name = name.Insert(position, "&");
                name = name.Insert(position, " ");
            }
            if (name.Contains("w/"))
            {
                int position = name.IndexOf("w/");
                name = name.Remove(position, "w/".Length);
                name = name.Insert(position, "&");
            }
            if (name.Contains(" w "))
            {
                int position = name.IndexOf(" w ");
                name = name.Remove(position, " w ".Length);
                name = name.Insert(position, " & ");
            }

            double comp;
            string tableName;

            //Make first letters to uppercase
            var newnNames = name.ToCharArray();
            var names = newnNames.ToList();
            if (names.Count >= 1)
            {
                if (char.IsLower(names[0]))
                {
                    names[0] = char.ToUpper(names[0]);
                }
            }
            for (int i = 1; i < names.Count; i++)
            {
                if (names[i - 1] == ' ')
                {
                    if (char.IsLower(names[i]))
                    {
                        names[i] = char.ToUpper(names[i]);
                    }
                }
                if (names[i - 1] == '&')
                {
                    if (names[i] != ' ')
                    {
                        names.Insert(i, ' ');
                    }
                }
                if (names[i] == '&')
                {
                    if (names[i - 1] != ' ')
                    {
                        names.Insert(i, ' ');
                    }
                }
            }
            name = string.Join("", names);

            //Compare with tableentries
            foreach (var food in _lunchBagContext.Food.ToList())
            {
                tableName = food.Name;
                comp = CompareNames(name, tableName);
                if (comp > 0.90)
                {
                    name = tableName;
                    break;
                }
            }

            return name;
        }

        private double CompareNames(string name, string tableName)
        {
            int result;
            if ((name == null) || (tableName == null)) result = 0;
            if ((name.Length == 0) || (tableName.Length == 0)) result = 0;
            if (name == tableName) result = name.Length;

            int sourceWordCount = name.Length;
            int targetWordCount = tableName.Length;

            // Step 1
            if (sourceWordCount == 0)
                result = targetWordCount;

            if (targetWordCount == 0)
                result = sourceWordCount;

            int[,] distance = new int[sourceWordCount + 1, targetWordCount + 1];

            // Step 2
            for (int i = 0; i <= sourceWordCount; distance[i, 0] = i++) ;
            for (int j = 0; j <= targetWordCount; distance[0, j] = j++) ;

            for (int i = 1; i <= sourceWordCount; i++)
            {
                for (int j = 1; j <= targetWordCount; j++)
                {
                    // Step 3
                    int cost = (tableName[j - 1] == name[i - 1]) ? 0 : 1;

                    // Step 4
                    distance[i, j] = Math.Min(Math.Min(distance[i - 1, j] + 1, distance[i, j - 1] + 1), distance[i - 1, j - 1] + cost);
                }
            }
            result = distance[sourceWordCount, targetWordCount];

            return (1.0 - ((double)result / (double)Math.Max(name.Length, tableName.Length)));
        }

        private int GetMonth(string _Month)
        {
            return _Month switch
            {
                "January" => 01,
                "February" => 02,
                "March" => 03,
                "April" => 04,
                "May" => 05,
                "June" => 06,
                "July" => 07,
                "August" => 08,
                "September" => 09,
                "October" => 10,
                "November" => 11,
                "December" => 12,
                _ => 00,
            };
        }
    }
}
