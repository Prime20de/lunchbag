﻿using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using LunchBagApp.Entities;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace LunchBagApp.Data
{
    public class TimedHostedService : IHostedService, IDisposable
    {
        private Timer _timer;
        public IServiceProvider Services { get; }

        private readonly ILogger<TimedHostedService> _logger;

        public TimedHostedService(IServiceProvider services, ILogger<TimedHostedService> logger)
        {
            Services = services;
            _logger = logger;
        }

        public void Dispose()
        {
            _timer?.Dispose();
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            _timer = new Timer(DoWork, null, TimeSpan.Zero, TimeSpan.FromHours(1));

            return Task.CompletedTask;
        }

        private void DoWork(object state)
        {
            /*
             * INFORMATION REMOVED
             * After each day has passed, it will disappear from the menu.
             * That means we can only get the full menu on Mondays!
             * We should keep trying to get the menu on the other days too, just in case something changes at some point.
             */
            if (DateTime.Today.DayOfWeek == DayOfWeek.Monday)
            {
                try
                {
                    using var scope = Services.CreateScope();
                    var lunchBagContext = scope.ServiceProvider.GetRequiredService<LunchBagContext>();
                    var foodUpdate = new FoodUpdate(lunchBagContext);
                    foodUpdate.UpdateFood(0);
                }
                catch (Exception e)
                {
                    _logger.LogError(e.ToString());
                }
            }
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            _timer?.Change(Timeout.Infinite, 0);

            return Task.CompletedTask;
        }
    }
}
