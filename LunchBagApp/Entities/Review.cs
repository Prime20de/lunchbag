﻿using System;
using System.Collections.Generic;

namespace LunchBagApp.Entities
{
    public partial class Review
    {
        public Review()
        {
            ReviewHelpful = new HashSet<ReviewHelpful>();
        }

        public int Id { get; set; }
        public int UserId { get; set; }
        public int WDFId { get; set; }
        public int Rating { get; set; }
        public string Comment { get; set; }
        public int Helpful { get; set; }
        public DateTime Changed { get; set; }
        public bool Edited { get; set; }

        public virtual WeekDayFood WDF { get; set; }
        public virtual User User { get; set; }
        public virtual ICollection<ReviewHelpful> ReviewHelpful { get; set; }
    }
}
