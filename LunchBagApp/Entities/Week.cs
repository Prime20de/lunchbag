﻿using System;
using System.Collections.Generic;

namespace LunchBagApp.Entities
{
    public partial class Week
    {
        public Week()
        {
            WeekDayFood = new HashSet<WeekDayFood>();
        }

        public int Id { get; set; }
        public DateTime StartDate { get; set; }

        public virtual ICollection<WeekDayFood> WeekDayFood { get; set; }
    }
}
