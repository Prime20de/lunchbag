﻿using System;
using System.Collections.Generic;

namespace LunchBagApp.Entities
{
    public partial class User
    {
        public User()
        {
            Favorite = new HashSet<Favorite>();
            Review = new HashSet<Review>();
            ReviewHelpful = new HashSet<ReviewHelpful>();
        }

        public int Id { get; set; }
        public string UniqueId { get; set; }
        public string Nickname { get; set; }
        public string SettingName { get; set; }
        public string SettingValue { get; set; }

        public virtual ICollection<Favorite> Favorite { get; set; }
        public virtual ICollection<Review> Review { get; set; }
        public virtual ICollection<ReviewHelpful> ReviewHelpful { get; set; }
    }
}