﻿using System;
using System.Collections.Generic;

namespace LunchBagApp.Entities
{
    public partial class Day
    {
        public Day()
        {
            WeekDayFood = new HashSet<WeekDayFood>();
        }

        public int Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<WeekDayFood> WeekDayFood { get; set; }
    }
}
