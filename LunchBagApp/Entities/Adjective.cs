﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LunchBagApp.Entities
{
    public class Adjective
    {
        public Adjective()
        {

        }

        public string Name { get; set; }
        public int Id { get; set; }
    }
}
