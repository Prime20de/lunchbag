﻿using System;
using System.Collections.Generic;

namespace LunchBagApp.Entities
{
    public partial class Favorite
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public int FoodId { get; set; }
        public bool IsFavorite { get; set; }

        public virtual Food Food { get; set; }
        public virtual User User { get; set; }
    }
}
