﻿using System;
using System.Collections.Generic;

namespace LunchBagApp.Entities
{
    public partial class Food
    {
        public Food()
        {
            Favorite = new HashSet<Favorite>();
            WeekDayFood = new HashSet<WeekDayFood>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public decimal OverallRating { get; set; }
        public int CategoryId { get; set; }
        public string Price { get; set; }

        public virtual Category Category { get; set; }
        public virtual ICollection<Favorite> Favorite { get; set; }
        public virtual ICollection<WeekDayFood> WeekDayFood { get; set; }
    }
}
