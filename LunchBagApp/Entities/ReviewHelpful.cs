﻿using System;
using System.Collections.Generic;

namespace LunchBagApp.Entities
{
    public partial class ReviewHelpful
    {
        public int ReviewId { get; set; }
        public int UserId { get; set; }
        public bool Helpful { get; set; }

        public virtual Review Review { get; set; }
        public virtual User User { get; set; }
    }
}
