﻿using System;
using System.Collections.Generic;

namespace LunchBagApp.Entities
{
    public partial class WeekDayFood
    {
        public WeekDayFood()
        {
            Review = new HashSet<Review>();
        }

        public int Id { get; set; }
        public int WeekId { get; set; }
        public int DayId { get; set; }
        public int FoodId { get; set; }

        public virtual Day Day { get; set; }
        public virtual Food Food { get; set; }
        public virtual Week Week { get; set; }

        public virtual ICollection<Review> Review { get; set; }
    }
}
