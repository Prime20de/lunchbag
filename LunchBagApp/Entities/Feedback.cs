﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LunchBagApp.Entities
{
    public class Feedback
    {
        public Feedback()
        {

        }
        public int Id { get; set; }
        public string Text { get; set; }
        public bool Checked { get; set; }
        public string Note { get; set; }
    }
}
